#include "Personnage.h"

Personnage::Personnage(const Corps &base) : Corps(base)
{
    //ctor
}

void Personnage::calculInputs(Input nosInputs)
{
    float ax = 0.0;
    float ay = 0.0;

    if(nosInputs.getTouche(SDL_SCANCODE_UP))
    {
        ay -= 1.0;
    }
    if(nosInputs.getTouche(SDL_SCANCODE_DOWN))
    {
        ay += 1.0;
    }
    if(nosInputs.getTouche(SDL_SCANCODE_LEFT))
    {
        ax -= 1.0;
    }
    if(nosInputs.getTouche(SDL_SCANCODE_RIGHT))
    {
        ax += 1.0;
    }

    float norme = sqrt(ax * ax + ay * ay);

    if(norme != 0)
    {
        ax = (ax / norme) * ACCELERATIONJOUEUR;
        ay = (ay / norme) * ACCELERATIONJOUEUR;
    }

    setAcceleration(ax, ay);
}
