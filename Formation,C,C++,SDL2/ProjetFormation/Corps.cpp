#include "Corps.h"

using namespace std;

Corps::Corps(float rayon, float x, float y, float coeff_vit) :
    m_rayon(rayon), m_x(x), m_y(y), m_coeff_vit(coeff_vit), m_vx(0), m_vy(0), m_ax(0), m_ay(0)
{

}

void Corps::gereDeplacement(float dt){

    calcVitesse(dt);
    calcPosition(dt);
    setAcceleration(0, 0);

}

/*void Corps::intersect(float *x1, float *y1, float *x2, float *y2, float xc1, float yc1, float xc2, float yc2, float b, float c)
{
    float a;
    float d;

    float A;
    float B;
    float C;

    float D;

    if(yc2 != yc1)
    {
        a = (-pow(xc1, 2) - pow(yc1, 2) + pow(xc2, 2) + pow(yc2, 2) + pow(b, 2) - pow(c, 2))/(2.0 * (yc2 - yc1));
        d = (xc2 - xc1)/(yc2 - yc1);

        A = (pow(d, 2) + 1);
        B = (-2 * xc1 + 2 * yc1 * d - 2 * a * d);
        C = pow(xc1, 2) + pow(yc1, 2) - 2 * yc1 * a + pow(a, 2) - pow(b, 2);

        D = pow(B, 2) - 4 * A * C;

        *x1 = (-B + sqrt(D))/(2 * A);
        *y1 = a - *x1 * d;

        *x2 = (-B - sqrt(D))/(2 * A);
        *y2 = a - *x2 * d;
    }
    else if (xc2 != xc1)
    {
        *x1 = (pow(c, 2) - pow(b, 2) - pow(xc2, 2) + pow(xc1, 2))/(2.0 * (xc1 - xc2));
        *x2 = *x1;

        A = 1;
        B = - 2.0 * yc2;
        C = pow(xc2, 2) + pow(*x1, 2) - 2.0 * xc2 * *x1 + pow(yc2, 2) - pow(c, 2);

        D = pow(B, 2) - 4 * A * C;

        *y1 = (-B + sqrt(D))/(2.0 * A);
        *y2 = (-B - sqrt(D))/(2.0 * A);
    }
    else
    {
        *x1 = NULL;
        *y1 = NULL;
        *x2 = NULL;
        *y2 = NULL;
    }
}*/

// Calcule la vitesse du Corps en connaissant sa vitesse actuelle et son acceleration
// dt = intervalle entre 2 maj de la vitesse
void Corps::calcVitesse(float dt){

    m_vx += m_ax * dt;
    m_vy += m_ay * dt;

    m_vx = m_vx * m_coeff_vit;
    m_vy = m_vy * m_coeff_vit;
}


void Corps::calcPosition(float dt) {

    m_x += m_vx * dt;
    m_y += m_vy * dt;
}

float Corps::getRayon()
{
    return m_rayon;
}

float Corps::getPositionX()
{
    return m_x;
}

float Corps::getPositionY()
{
    return m_y;
}

void Corps::setAcceleration(float ax, float ay) {

	m_ax = ax;
	m_ay = ay;
}
