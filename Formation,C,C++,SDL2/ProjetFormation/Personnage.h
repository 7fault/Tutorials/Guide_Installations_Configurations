#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include "Corps.h"
#include "Input.h"

#define ACCELERATIONJOUEUR 0.005

using namespace std;

class Personnage : public Corps
{
    public:

        Personnage(const Corps &base);

        void calculInputs(Input nosInputs);
};

#endif // PERSONNAGE_H
