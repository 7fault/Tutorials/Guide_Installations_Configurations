#ifndef DEF_INPUT
#define DEF_INPUT

// Include

#include <iostream>
#include <vector>
#include <SDL2/SDL.h>

// Define Global

#define MAXJOYS 4

// Defines Manette Xbox
// Dead zone de la manette pour �viter les mouvements involontaires
#define DEAD_ZONE 8000

// Defines des mapping des boutons
#define BOUTON_HAUT 0
#define BOUTON_BAS 1
#define BOUTON_GAUCHE 2
#define BOUTON_DROITE 3

#define BOUTON_START 4
#define BOUTON_BACK 5

#define BOUTON_PADG 6
#define BOUTON_PADD 7

#define BOUTON_GACHETTEG 8
#define BOUTON_GACHETTED 9

#define BOUTON_A 10
#define BOUTON_B 11
#define BOUTON_X 12
#define BOUTON_Y 13

#define NBR_BOUTONS_MANETTES 14

// Classe

class Input
{
    public:

        Input();
        ~Input();

        void updateEvenements();
        bool terminer() const;
        void afficherPointeur(bool reponse) const;
        void capturerPointeur(bool reponse) const;

        bool getTouche(const SDL_Scancode touche) const;
        bool getBoutonManette(const int bouton, const int manette = 0) const;
        bool getBoutonSouris(const Uint8 bouton) const;
        bool mouvementSouris() const;

        int getX() const;
        int getY() const;

        int getXRel() const;
        int getYRel() const;


    private:

        // Joystick

        void openJoysticks();
        void closeJoysticks();

        int getNumJoysticks() const;

        //Variiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiables

        SDL_Event m_evenements;
        bool m_touches[SDL_NUM_SCANCODES];
        bool m_boutonsSouris[8];

        int m_x;
        int m_y;
        int m_xRel;
        int m_yRel;

        bool m_terminer;

        // Joystick
        SDL_Joystick* m_joysticks[MAXJOYS];
        int m_nbrJoys;

        bool m_DPADsinUse[MAXJOYS];

        std::vector< std::vector<bool> > m_boutonsManettes;
};

#endif

