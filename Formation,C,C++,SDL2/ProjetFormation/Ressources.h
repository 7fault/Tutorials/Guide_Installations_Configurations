#ifndef RESSOURCES_H_INCLUDED
#define RESSOURCES_H_INCLUDED

#include <iostream>
#include <vector>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>

#define FENETRE_LARGEUR 800
#define FENETRE_HAUTEUR 600

#define TRUC 0

#define NBSPRITE 1

using namespace std;

class Ressources
{
    public :

        Ressources(); // Param�tres de la fen�tre ? Pourquoi pas ...
        virtual ~Ressources();

        void remplir(int n_texture, int x, int y , int dx, int dy, int alpha = 255, float angle = 0.0);

        void afficher();

    private :

        int ouvrirFenetre();
        void detruireFenetre();

        SDL_Texture* creationTexture(char* texte);

        void chargementTexturesSons();
        void detruireTexturesSons();

        // Attributs

        SDL_Window *m_fenetre;
        SDL_Renderer *m_renderer;

        // Textures

        SDL_Texture* m_textures[NBSPRITE];
};

#endif
