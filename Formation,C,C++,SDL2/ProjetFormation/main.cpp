#include "Ressources.h"
#include "Personnage.h"

int main(int argc, char *argv[])
{
    // Cr�ation des trucs n�cessaire quoi qu'il arrive.
    Ressources nosRessources;
    Input nosInputs;

    // Cr�ation des donn�es pour le jeu lui m�me
    Personnage joueur(Corps(20.0, 0, 0));

    while(!nosInputs.getTouche(SDL_SCANCODE_ESCAPE))
    {
        // Qu'est-ce que le joueur d�cide ?
        nosInputs.updateEvenements();
        joueur.calculInputs(nosInputs);

        // R�sultat de la d�cision
        joueur.gereDeplacement(1000.0 / 60.0);

        // Affichage
        nosRessources.remplir(TRUC, joueur.getPositionX(), joueur.getPositionY(),
                              2 * joueur.getRayon(), 2 * joueur.getRayon());
        nosRessources.afficher();

        // On lib�re le processeur
        SDL_Delay(1000.0 / 60.0);
    }
}
