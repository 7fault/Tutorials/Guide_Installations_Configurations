#ifndef Corps_H
#define Corps_H

#include <vector>
#include <math.h>
#include <iostream>

using namespace std;

class Corps
{
    public:

        Corps(float rayon, float x, float y, float coeff_vit = 0.9);

		void gereDeplacement(float dt);

		float getRayon();

		float getPositionX();
		float getPositionY();

		void setAcceleration(float ax, float ay);

    private:

        // Fonctions pour le déplacement
		void calcVitesse(float dt);
		void calcPosition(float dt);

		// Attributs
		float m_rayon;

        float m_coeff_vit;

        float m_x;
        float m_y;

        float m_vx;
        float m_vy;

        float m_ax;
        float m_ay;
};

#endif // Corps_H
