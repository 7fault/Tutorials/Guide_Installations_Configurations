#include "Input.h"

using namespace std;

// Constructeur et Destructeur

Input::Input() : m_x(0), m_y(0), m_xRel(0), m_yRel(0), m_terminer(false), m_nbrJoys(0)
{
    // Initialisation du tableau m_touches[]

    for(int i(0); i < SDL_NUM_SCANCODES; i++)
        m_touches[i] = false;


    // Initialisation du tableau m_boutonsSouris[]

    for(int i(0); i < 8; i++)
        m_boutonsSouris[i] = false;

    // Initialisation m_boutonsManettes
    closeJoysticks();
    for(int rang(0); rang < SDL_NumJoysticks(); rang++)
    {
        openJoysticks();
    }

    for(unsigned int rang(0); rang < m_boutonsManettes.size(); rang++)
    {
        for(unsigned int rang2(0); rang2 < NBR_BOUTONS_MANETTES; rang2++)
        {
            m_boutonsManettes[rang][rang2] = false;
        }
    }
}


Input::~Input()
{

}


// M�thodes

void Input::updateEvenements()
{
    // Pour �viter des mouvements fictifs de la souris, on r�initialise les coordonn�es relatives

    m_xRel = 0;
    m_yRel = 0;


    // Boucle d'�v�nements

    while(SDL_PollEvent(&m_evenements))
    {
        // Switch sur le type d'�v�nement

        switch(m_evenements.type)
        {
            // Cas d'une touche enfonc�e

            case SDL_KEYDOWN:
                m_touches[m_evenements.key.keysym.scancode] = true;
            break;


            // Cas d'une touche rel�ch�e

            case SDL_KEYUP:
                m_touches[m_evenements.key.keysym.scancode] = false;
            break;


            // Cas de pression sur un bouton de la souris

            case SDL_MOUSEBUTTONDOWN:

                m_boutonsSouris[m_evenements.button.button] = true;

            break;


            // Cas du rel�chement d'un bouton de la souris

            case SDL_MOUSEBUTTONUP:

                m_boutonsSouris[m_evenements.button.button] = false;

            break;


            // Cas d'un mouvement de souris

            case SDL_MOUSEMOTION:

                m_x = m_evenements.motion.x;
                m_y = m_evenements.motion.y;

                m_xRel = m_evenements.motion.xrel;
                m_yRel = m_evenements.motion.yrel;

            break;

            // Cas des manettes
            case SDL_JOYDEVICEADDED:

                //initialisation();
                //initialisationManettes();

            break;

            case SDL_JOYDEVICEREMOVED:

                //initialisation();
                //initialisationManettes();

            break;

            // Cas d'un bouton enfonc�e
            case SDL_JOYBUTTONDOWN:

                if (m_evenements.jbutton.which < getNumJoysticks())
                {
                    m_boutonsManettes[m_evenements.jbutton.which][m_evenements.jbutton.button] = true;
                }

            break;

            // Cas d'un bouton rel�ch�e
            case SDL_JOYBUTTONUP:

                if (m_evenements.jbutton.which < getNumJoysticks())
                {
                    m_boutonsManettes[m_evenements.jbutton.which][m_evenements.jbutton.button] = false;
                }

            break;

            // Cas d'un mouvement de Thumpad
            case SDL_JOYAXISMOTION:

                 //Gestion du thumbpad, seulement si on n'utilise pas d�j� le D-PAD
                if (m_evenements.type == SDL_JOYAXISMOTION && m_evenements.jaxis.which < getNumJoysticks() /*&& m_DPADsinUse[m_evenements.jaxis.which] == false*/)
                {
                    //Si le joystick a d�tect� un mouvement
                    //if (m_evenements.jaxis.wich == 0)
                    //{
                        //Si le mouvement a eu lieu sur l'axe des X
                        if (m_evenements.jaxis.axis == 0)
                        {
                            //Si l'axe des X est neutre ou � l'int�rieur de la "dead zone"
                            if ((m_evenements.jaxis.value > -DEAD_ZONE) && (m_evenements.jaxis.value < DEAD_ZONE))
                            {
                                m_boutonsManettes[m_evenements.jaxis.which][BOUTON_GAUCHE] = false;
                                m_boutonsManettes[m_evenements.jaxis.which][BOUTON_DROITE] = false;
                            }
                            //Sinon, de quel c�t� va-t-on ?
                            else
                            {
                                //Si sa valeur est n�gative, on va � gauche
                                if (m_evenements.jaxis.value < 0)
                                {
                                    m_boutonsManettes[m_evenements.jaxis.which][BOUTON_GAUCHE] = true;
                                    m_boutonsManettes[m_evenements.jaxis.which][BOUTON_DROITE] = false;
                                }
                                //Sinon, on va � droite
                                else
                                {
                                    m_boutonsManettes[m_evenements.jaxis.which][BOUTON_GAUCHE] = false;
                                    m_boutonsManettes[m_evenements.jaxis.which][BOUTON_DROITE] = true;
                                }
                            }
                        }

                        //Si le mouvement a eu lieu sur l'axe des Y
                        else if (m_evenements.jaxis.axis == 1)
                        {
                            //Si l'axe des Y est neutre ou � l'int�rieur de la "dead zone"
                            if ((m_evenements.jaxis.value > -DEAD_ZONE) && (m_evenements.jaxis.value < DEAD_ZONE))
                            {
                                m_boutonsManettes[m_evenements.jaxis.which][BOUTON_HAUT] = false;
                                m_boutonsManettes[m_evenements.jaxis.which][BOUTON_BAS] = false;
                            }
                            //Sinon, de quel c�t� va-t-on ?
                            else
                            {
                                //Si sa valeur est n�gative, on va en haut
                                if (m_evenements.jaxis.value < 0)
                                {
                                    m_boutonsManettes[m_evenements.jaxis.which][BOUTON_HAUT] = true;
                                    m_boutonsManettes[m_evenements.jaxis.which][BOUTON_BAS] = false;
                                }
                                //Sinon, on va en bas
                                else
                                {
                                    m_boutonsManettes[m_evenements.jaxis.which][BOUTON_HAUT] = false;
                                    m_boutonsManettes[m_evenements.jaxis.which][BOUTON_BAS] = true;
                                }
                            }
                        }
                    //}
                }

            break;


            // Cas de la fermeture de la fen�tre

            case SDL_WINDOWEVENT:

                if(m_evenements.window.event == SDL_WINDOWEVENT_CLOSE)
                    m_terminer = true;

            break;


            default:
            break;
        }
    }
}


bool Input::terminer() const
{
    return m_terminer;
}


void Input::afficherPointeur(bool reponse) const
{
    if(reponse)
        SDL_ShowCursor(SDL_ENABLE);

    else
        SDL_ShowCursor(SDL_DISABLE);
}


void Input::capturerPointeur(bool reponse) const
{
    if(reponse)
        SDL_SetRelativeMouseMode(SDL_TRUE);

    else
        SDL_SetRelativeMouseMode(SDL_FALSE);
}



// Getters

bool Input::getTouche(const SDL_Scancode touche) const
{
    return m_touches[touche];
}

bool Input::getBoutonManette(const int bouton, const int manette) const
{
    if (manette < m_boutonsManettes.size())
    {
        return m_boutonsManettes[manette][bouton];
    }
    else
    {
        return false;
    }
}

bool Input::getBoutonSouris(const Uint8 bouton) const
{
    return m_boutonsSouris[bouton];
}


bool Input::mouvementSouris() const
{
    if(m_xRel == 0 && m_yRel == 0)
        return false;

    else
        return true;
}


// Getters concernant la position du curseur

int Input::getX() const
{
    return m_x;
}

int Input::getY() const
{
    return m_y;
}

int Input::getXRel() const
{
    return m_xRel;
}

int Input::getYRel() const
{
    return m_yRel;
}

void Input::openJoysticks(void)
{
    if (SDL_NumJoysticks() > getNumJoysticks())
    {
        //On ouvre le joystick
        m_joysticks[getNumJoysticks()] = SDL_JoystickOpen(getNumJoysticks());

        if (!m_joysticks[getNumJoysticks()])
        {
            cout << "Le joystick " << getNumJoysticks() << " n'a pas pu �tre ouvert !" << endl;
        }
        else
        {
            //cout << "Le joystick " << getNumJoysticks() << " a ete ouvert !" << endl;
            m_nbrJoys++;
        }

        //on initialise la table d'inputs
        vector<bool> construct;

        for (unsigned int rang = 0; rang < NBR_BOUTONS_MANETTES; rang++)
        {
            construct.push_back(false);
        }

        m_boutonsManettes.push_back(construct);
    }
}

void Input::closeJoysticks(void)
{
    while (getNumJoysticks() > 0)
    {
        if (SDL_JoystickGetAttached(m_joysticks[getNumJoysticks() - 1]))
        {
            SDL_JoystickClose(m_joysticks[getNumJoysticks() - 1]);
            //std::cout << "Close Joystick " << getNumJoysticks() - 1 << std::endl;
            m_nbrJoys--;
        }
        else
        {
            //SDL_JoystickClose(m_joysticks[getNumJoysticks() - 1]);
            m_nbrJoys--;
        }
    }

    m_boutonsManettes.clear();
}

int Input::getNumJoysticks() const
{
    return m_nbrJoys;
}
