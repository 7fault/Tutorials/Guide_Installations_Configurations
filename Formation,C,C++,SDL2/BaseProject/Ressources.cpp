#include "Ressources.h"

Ressources::Ressources()
{
    ouvrirFenetre();
    chargementTexturesSons();
}

Ressources::~Ressources()
{
    detruireTexturesSons();
    detruireFenetre();
}

void Ressources::remplir(int nTexture, int x, int y ,int dx,int dy, int alpha, float angle)
{

    if (nTexture > NBSPRITE || nTexture < 0)
    {

        std::cout <<"Erreur : "<< nTexture << "l'image demand� n'est pas cr��e" << std::endl;
    }
    else
    {
        SDL_SetTextureAlphaMod(m_textures[nTexture],alpha);
        SDL_Rect rect = {.x=x, .y=y, .w=dx, .h=dy};
        SDL_RenderCopyEx(m_renderer, m_textures[nTexture], NULL, &rect, angle, NULL, SDL_FLIP_NONE);
    }
}


void Ressources::afficher()
{
    SDL_RenderPresent(m_renderer);
    SDL_RenderClear(m_renderer);
}

int Ressources::ouvrirFenetre()
{
    m_fenetre = SDL_CreateWindow("Formation", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                               FENETRE_LARGEUR, FENETRE_HAUTEUR, SDL_WINDOW_SHOWN
                               //|| SDL_WINDOW_FULLSCREEN
                               );

    if(!m_fenetre)
    {
        std::cout << "SDL Error : " << SDL_GetError() << std::endl;
        return -1;
    }

    m_renderer = SDL_CreateRenderer(m_fenetre, -1, 0);

    if(!m_renderer)
    {
        std::cout << "SDL Error : " << SDL_GetError() << std::endl;
        return -1;
    }

    SDL_InitSubSystem(SDL_INIT_JOYSTICK); //init joysticks

    return 0;
}

void Ressources::detruireFenetre()
{
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyWindow(m_fenetre);
    SDL_Quit();
}

SDL_Texture* Ressources::creationTexture(char* texte)
{
    SDL_Surface* tmp = IMG_Load(texte);

    SDL_Texture* retour = SDL_CreateTextureFromSurface(m_renderer, tmp);

    SDL_FreeSurface(tmp);

    return retour;
}

void Ressources::chargementTexturesSons()
{
     //Initialize PNG loading
    int imgFlags = IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) )
    {
        std::cout << "SDL_image could not initialize! SDL_image Error: %s\n" << IMG_GetError() << std::endl;
    }

    // Chargement des textures
    m_textures[0] = creationTexture("Ressources/Truc.png");
}

void Ressources::detruireTexturesSons()
{
    unsigned int rang;
    for (rang = 0; rang < NBSPRITE; rang++)
    {
        if(m_textures[rang] != NULL)
        {
            SDL_DestroyTexture(m_textures[rang]);
        }
    }

    IMG_Quit();
}
