#include "Ressources.h"
#include "Input.h"

int main(int argc, char *argv[])
{
    // Cr�ation des trucs n�cessaire quoi qu'il arrive.
    Ressources nosRessources;
    Input nosInputs;

    while(!nosInputs.getTouche(SDL_SCANCODE_ESCAPE))
    {
        // On met � jour les inputs
        nosInputs.updateEvenements();

        // Affichage
        nosRessources.remplir(TRUC, FENETRE_LARGEUR/2 - 20, FENETRE_HAUTEUR/2 - 40,
                              40, 80);

        nosRessources.afficher();

        // On lib�re le processeur
        SDL_Delay(1000.0 / 60.0);
    }
}
