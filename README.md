# Guide Installations Configurations

## Liste des Guides:

### C++ Compilation Library:

**Formation C++ Compilation IDE** : présentation pdf - par Arnaud Billon

**Rappels C Inclusion Library** : pdf - par Arnaud Billon

**CMake Compiler sources de library** : instructions .txt - par Arnaud Billon

**Installation SDL2 Windows CodeBlocks** : instructions .txt - par Corentin Le Lez

### C++ Basics:

**Formation C/C++/SDL2** : projet windows/codeblocks all-in-one - par Rémi Monthiller

### Tools:

**Formation GIT** : présentation pdf - par Arnaud Billon

**Installation configuration git** : pdf - par Arnaud Billon




